﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sdsystem
{
    public partial class FrmConfigSD : Form
    {
        public FrmConfigSD()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void FrmConfigSD_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            panel1.Width = this.Width;
            LibDisplay ld = new LibDisplay();
            Libshowdata libShData = new Libshowdata();

            ld.LoadProvince(cbbprovince, 0);
            libShData.LoadDataSD(dataSD);

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void cbbprovince_SelectedIndexChanged(object sender, EventArgs e)
        {
            LibDisplay ld = new LibDisplay();
            string Prov = cbbprovince.SelectedValue.ToString();
            // int ProvId = Int32.Parse(Prov);
              ld.LoadAumphur(cbbaumphur, Prov);
           
        }

        private void cbbaumphur_SelectedIndexChanged(object sender, EventArgs e)
        {
            LibDisplay ld = new LibDisplay();
            string Aumphur = cbbaumphur.SelectedValue.ToString();
           
            ld.LoadTumbon(cbbtumbon, Aumphur);
            ld.LoadPostcode(txtpostcode,Aumphur);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sdname = txtsdname.Text;
            string sdarmybook = txtarmybook.Text;
            string sdgovbook = txtgovbook.Text;
            string sdtel = txttel.Text;
            string sdbanid = txtbanid.Text;
            string sdroad = txtroad.Text;
            string sdsoy = txtsoy.Text;
            string sdmoo = txtmoo.Text;
            string sdprov = cbbprovince.SelectedValue.ToString();
            string sdamphur = cbbaumphur.SelectedValue.ToString();
            string sdtumbon = cbbtumbon.SelectedValue.ToString();
            string sdpostcode = txtpostcode.Text;

            if(sdname =="" || sdarmybook=="" || sdgovbook=="" || sdtel=="" || sdbanid=="" || sdroad=="" || sdsoy=="" || sdmoo ==""
                || sdprov =="0" || sdamphur =="0" || sdtumbon=="0" || sdpostcode=="")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบทุกช่องด้วย!", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                LibSQL Connection = new LibSQL();
                Libshowdata libShData = new Libshowdata();
                Connection.OpenConnection();
                
                string StrSql = "insert into tb_configsd (sdname,sdarmybook,sdgovbook,sdtel,sdbanid,sdroad," +
                    "sdsoy,sdmoo,sdprovince,sdamphur,sdtumbon,sdpostcode) values" +
                    "('" + sdname + "','" + sdarmybook + "','" + sdgovbook + "','" + sdtel + "','" + sdbanid + "','" + sdroad + "'," +
                    " '"+sdsoy+"','"+sdmoo+"','"+sdprov+"','"+sdamphur+"','"+sdtumbon+"','"+sdpostcode+"') ";
                MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
                MySqlDataReader reader;
                reader = cmd.ExecuteReader();
                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว!", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                libShData.LoadDataSD(dataSD);
               // MessageBox.Show(StrSql);
            }

        }
    }
}
