﻿namespace sdsystem
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.toolStripInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusDateTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ConfigSDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BtConfigPersonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BtHomeNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.แบบพมพสด44ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.แบบพมพสด1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.แบบพมพสด2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.แบบพมพสด3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.แบบพมพสด9ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuBtregister = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.BtRegisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ตรวจสอบการลงบญชToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.แจงผลการตรวจสอบฯToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DataRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Data16ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Data18ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.การลงบญชประจำเดอนToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จบการทำงานToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.MenuBtregister,
            this.editMenu,
            this.viewMenu,
            this.helpMenu,
            this.จบการทำงานToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(950, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripInfo,
            this.StatusUser,
            this.StatusDateTime,
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(950, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // toolStripInfo
            // 
            this.toolStripInfo.Image = global::sdsystem.Properties.Resources.social_facebook_icon;
            this.toolStripInfo.Name = "toolStripInfo";
            this.toolStripInfo.Size = new System.Drawing.Size(249, 16);
            this.toolStripInfo.Text = "ออกแบบและพัฒนาระบบโดย สิบเอกสนธยา อุปถัมภ์ ";
            // 
            // StatusUser
            // 
            this.StatusUser.Image = global::sdsystem.Properties.Resources.Army_Officer_icon;
            this.StatusUser.Name = "StatusUser";
            this.StatusUser.Size = new System.Drawing.Size(16, 16);
            // 
            // StatusDateTime
            // 
            this.StatusDateTime.Image = global::sdsystem.Properties.Resources.system_time_icon;
            this.StatusDateTime.Name = "StatusDateTime";
            this.StatusDateTime.Size = new System.Drawing.Size(16, 16);
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.toolStripSeparator4,
            this.printToolStripMenuItem,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem});
            this.fileMenu.Image = global::sdsystem.Properties.Resources.Actions_system;
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(82, 20);
            this.fileMenu.Text = "&ตั้งค่าระบบ";
            this.fileMenu.Click += new System.EventHandler(this.fileMenu_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ConfigSDToolStripMenuItem,
            this.BtConfigPersonToolStripMenuItem,
            this.BtHomeNameToolStripMenuItem});
            this.newToolStripMenuItem.Image = global::sdsystem.Properties.Resources.Actions_system;
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShowShortcutKeys = false;
            this.newToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.newToolStripMenuItem.Text = "&ตั้งค่าพื้นฐานระบบ";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.ShowNewForm);
            // 
            // ConfigSDToolStripMenuItem
            // 
            this.ConfigSDToolStripMenuItem.Name = "ConfigSDToolStripMenuItem";
            this.ConfigSDToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.ConfigSDToolStripMenuItem.Text = "ตั้งค่าหน่วยสัสดีอำเภอ";
            this.ConfigSDToolStripMenuItem.Click += new System.EventHandler(this.ConfigSDToolStripMenuItem_Click);
            // 
            // BtConfigPersonToolStripMenuItem
            // 
            this.BtConfigPersonToolStripMenuItem.Name = "BtConfigPersonToolStripMenuItem";
            this.BtConfigPersonToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.BtConfigPersonToolStripMenuItem.Text = "ตั้งค่าข้อมูลกำลังพล";
            this.BtConfigPersonToolStripMenuItem.Click += new System.EventHandler(this.BtConfigPersonToolStripMenuItem_Click);
            // 
            // BtHomeNameToolStripMenuItem
            // 
            this.BtHomeNameToolStripMenuItem.Name = "BtHomeNameToolStripMenuItem";
            this.BtHomeNameToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.BtHomeNameToolStripMenuItem.Text = "ตั้งค่าชื่อหมู่บ้าน";
            this.BtHomeNameToolStripMenuItem.Click += new System.EventHandler(this.BtHomeNameToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(173, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.แบบพมพสด44ToolStripMenuItem,
            this.แบบพมพสด1ToolStripMenuItem,
            this.แบบพมพสด2ToolStripMenuItem,
            this.แบบพมพสด3ToolStripMenuItem,
            this.แบบพมพสด9ToolStripMenuItem});
            this.printToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripMenuItem.Image")));
            this.printToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShowShortcutKeys = false;
            this.printToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.printToolStripMenuItem.Text = "&ตั้งค่าแบบพิมพ์";
            // 
            // แบบพมพสด44ToolStripMenuItem
            // 
            this.แบบพมพสด44ToolStripMenuItem.Name = "แบบพมพสด44ToolStripMenuItem";
            this.แบบพมพสด44ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.แบบพมพสด44ToolStripMenuItem.Text = "แบบพิมพ์ สด.44";
            // 
            // แบบพมพสด1ToolStripMenuItem
            // 
            this.แบบพมพสด1ToolStripMenuItem.Name = "แบบพมพสด1ToolStripMenuItem";
            this.แบบพมพสด1ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.แบบพมพสด1ToolStripMenuItem.Text = "แบบพิมพ์ สด.1";
            // 
            // แบบพมพสด2ToolStripMenuItem
            // 
            this.แบบพมพสด2ToolStripMenuItem.Name = "แบบพมพสด2ToolStripMenuItem";
            this.แบบพมพสด2ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.แบบพมพสด2ToolStripMenuItem.Text = "แบบพิมพ์ สด.2";
            // 
            // แบบพมพสด3ToolStripMenuItem
            // 
            this.แบบพมพสด3ToolStripMenuItem.Name = "แบบพมพสด3ToolStripMenuItem";
            this.แบบพมพสด3ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.แบบพมพสด3ToolStripMenuItem.Text = "แบบพิมพ์ สด.3";
            // 
            // แบบพมพสด9ToolStripMenuItem
            // 
            this.แบบพมพสด9ToolStripMenuItem.Name = "แบบพมพสด9ToolStripMenuItem";
            this.แบบพมพสด9ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.แบบพมพสด9ToolStripMenuItem.Text = "แบบพิมพ์ สด.9";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(173, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::sdsystem.Properties.Resources.Turn_Off_icon_16;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.exitToolStripMenuItem.Text = "จบการทำงาน";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
            // 
            // MenuBtregister
            // 
            this.MenuBtregister.Image = global::sdsystem.Properties.Resources.Army_Officer_icon;
            this.MenuBtregister.Name = "MenuBtregister";
            this.MenuBtregister.Size = new System.Drawing.Size(131, 20);
            this.MenuBtregister.Text = "&แสดงตนเพื่อลงบัญชีฯ";
            this.MenuBtregister.Click += new System.EventHandler(this.MenuBtregister_Click);
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator6,
            this.BtRegisToolStripMenuItem,
            this.DataRegisterToolStripMenuItem});
            this.editMenu.Image = global::sdsystem.Properties.Resources.edit_icon;
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(93, 20);
            this.editMenu.Text = "&ระบบงานอื่นๆ";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(187, 6);
            // 
            // BtRegisToolStripMenuItem
            // 
            this.BtRegisToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ตรวจสอบการลงบญชToolStripMenuItem,
            this.แจงผลการตรวจสอบฯToolStripMenuItem});
            this.BtRegisToolStripMenuItem.Image = global::sdsystem.Properties.Resources.post_it_icon;
            this.BtRegisToolStripMenuItem.Name = "BtRegisToolStripMenuItem";
            this.BtRegisToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.BtRegisToolStripMenuItem.ShowShortcutKeys = false;
            this.BtRegisToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.BtRegisToolStripMenuItem.Text = "&ระบบตรวจสอบการลงบัญชีฯ";
            // 
            // ตรวจสอบการลงบญชToolStripMenuItem
            // 
            this.ตรวจสอบการลงบญชToolStripMenuItem.Name = "ตรวจสอบการลงบญชToolStripMenuItem";
            this.ตรวจสอบการลงบญชToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.ตรวจสอบการลงบญชToolStripMenuItem.Text = "&ขอตรวจสอบการลงบัญชีฯ";
            // 
            // แจงผลการตรวจสอบฯToolStripMenuItem
            // 
            this.แจงผลการตรวจสอบฯToolStripMenuItem.Name = "แจงผลการตรวจสอบฯToolStripMenuItem";
            this.แจงผลการตรวจสอบฯToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.แจงผลการตรวจสอบฯToolStripMenuItem.Text = "&แจ้งผลการตรวจสอบฯ";
            // 
            // DataRegisterToolStripMenuItem
            // 
            this.DataRegisterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Data16ToolStripMenuItem,
            this.Data18ToolStripMenuItem});
            this.DataRegisterToolStripMenuItem.Image = global::sdsystem.Properties.Resources.post_it_icon;
            this.DataRegisterToolStripMenuItem.Name = "DataRegisterToolStripMenuItem";
            this.DataRegisterToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.DataRegisterToolStripMenuItem.Text = "ระบบงานหมายเรียกฯ";
            // 
            // Data16ToolStripMenuItem
            // 
            this.Data16ToolStripMenuItem.Name = "Data16ToolStripMenuItem";
            this.Data16ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.Data16ToolStripMenuItem.Text = "จัดทำหมายเรียกประจำปี";
            // 
            // Data18ToolStripMenuItem
            // 
            this.Data18ToolStripMenuItem.Name = "Data18ToolStripMenuItem";
            this.Data18ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.Data18ToolStripMenuItem.Text = "รายงานวันรับหมายเรียก";
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.viewMenu.Image = global::sdsystem.Properties.Resources.reports_icon;
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(113, 20);
            this.viewMenu.Text = "&ระบบรายงานต่างๆ";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.การลงบญชประจำเดอนToolStripMenuItem,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.toolStripMenuItem1.Image = global::sdsystem.Properties.Resources.post_it_icon;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(158, 22);
            this.toolStripMenuItem1.Text = "&ข้อมูลการลงบัญชีฯ";
            // 
            // การลงบญชประจำเดอนToolStripMenuItem
            // 
            this.การลงบญชประจำเดอนToolStripMenuItem.Name = "การลงบญชประจำเดอนToolStripMenuItem";
            this.การลงบญชประจำเดอนToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.การลงบญชประจำเดอนToolStripMenuItem.Text = "&การลงบัญชีประจำเดือน";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(176, 22);
            this.toolStripMenuItem2.Text = "&การลงบัญชีตาม ม.16";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(176, 22);
            this.toolStripMenuItem3.Text = "&การลงบัญชีตาม ม.18";
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator8,
            this.aboutToolStripMenuItem});
            this.helpMenu.Image = global::sdsystem.Properties.Resources.help_about_icon;
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(98, 20);
            this.helpMenu.Text = "&ระบบช่วยเหลือ";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("indexToolStripMenuItem.Image")));
            this.indexToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("searchToolStripMenuItem.Image")));
            this.searchToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.searchToolStripMenuItem.Text = "&Search";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(165, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.aboutToolStripMenuItem.Text = "&About ... ...";
            // 
            // จบการทำงานToolStripMenuItem
            // 
            this.จบการทำงานToolStripMenuItem.Image = global::sdsystem.Properties.Resources.Turn_Off_icon_16;
            this.จบการทำงานToolStripMenuItem.Name = "จบการทำงานToolStripMenuItem";
            this.จบการทำงานToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.จบการทำงานToolStripMenuItem.Text = "&จบการทำงาน";
            this.จบการทำงานToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 453);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "ระบบสนับสนุนสายงานสัสดี";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem แบบพมพสด44ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem แบบพมพสด1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem แบบพมพสด2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem แบบพมพสด3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem แบบพมพสด9ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ConfigSDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BtConfigPersonToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel StatusUser;
        private System.Windows.Forms.ToolStripStatusLabel StatusDateTime;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripInfo;
        private System.Windows.Forms.ToolStripMenuItem จบการทำงานToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuBtregister;
        private System.Windows.Forms.ToolStripMenuItem DataRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Data16ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Data18ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BtRegisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ตรวจสอบการลงบญชToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem แจงผลการตรวจสอบฯToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem การลงบญชประจำเดอนToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BtHomeNameToolStripMenuItem;
    }
}



