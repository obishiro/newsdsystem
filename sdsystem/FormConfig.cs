﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;




namespace sdsystem
{
    public partial class FrmConfig : Form
    {
        public FrmConfig()
        {
            InitializeComponent();
        }

        private void btTestConnect_Click(object sender, EventArgs e)
        {
        
            string Host = txtHost.Text;
            string DBUser = txtDBUser.Text;
            string DBPassword = txtDBPassword.Text;
            string DBName = txtDBName.Text;
            string Port = txtDBPort.Text;
            // string strConnString;
        
            string connectionString;
            connectionString = "SERVER=" + Host + ";" + "DATABASE=" +
            DBName + ";" + "UID=" + DBUser + ";" + "PASSWORD=" + DBPassword + ";";

            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
           // MySqlDataReader reader;

            try
            {
                databaseConnection.Open();
              

                if (MessageBox.Show("เชื่อมต่อฐานข้อมูลสำเร็จ", "การเชื่อมต่อฐานข้อมูล", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    this.Visible = false;
                    databaseConnection.Close();
                }

               
            }
            catch (Exception ex)
            {
                // Show any error message.
                MessageBox.Show(ex.Message);
            }

        }
    }
}
