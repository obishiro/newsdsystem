﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace sdsystem
{
    public partial class FrmConfigPerson : Form
    {



        public FrmConfigPerson()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            this.Hide();
        }

        private void FrmConfigPerson_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            LibDisplay libShowDisplay = new LibDisplay();
            Libshowdata libshowdata = new Libshowdata();
            libShowDisplay.LoadYod(cbbYod);
            libShowDisplay.LoadProvince(cbbprovince,0);
           
            libShowDisplay.LoadPosition(cbbPosition);
            libshowdata.LoadDataPerson(dataPerson);
            panel1.Width = this.Width;
           

        }

        private void btSave_Click(object sender, EventArgs e)
        {
            string PID = txtPID.Text;
            string SID = txtSID.Text;
            string Name = txtName.Text;
            string LastName = txtLastName.Text;
            string Yod = cbbYod.SelectedValue.ToString();
            string Position = cbbPosition.SelectedValue.ToString();

            if (PID == "" || SID == "" || Name == "" || LastName == "" || Yod == "0" || Position == "0")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบทุกช่องด้วย!", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                LibSQL Connection = new LibSQL();
                Libshowdata libShowData = new Libshowdata();
                Connection.OpenConnection();
                
                string StrSql = "insert into tb_userinfo (pid,sid,yod,name,lastname,position) values" +
                    "('" + PID + "','" + SID + "','" + Yod + "','" + Name + "','" + LastName + "','" + Position + "') ";
                MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
                MySqlDataReader reader;
                reader = cmd.ExecuteReader();
                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว!", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                libShowData.LoadDataPerson(dataPerson);
                txtPID.Clear();
                txtSID.Clear();
                cbbYod.ResetText();
                txtName.Clear();
                txtLastName.Clear();
                cbbPosition.ResetText();
            }
           // Form frm = new FrmConfigPerson();
            
           // MessageBox.Show("" + StrSql);



        }

        private void cbbprovince_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                LibDisplay ld = new LibDisplay();
                string Prov = cbbprovince.SelectedValue.ToString();
                // int ProvId = Int32.Parse(Prov);
                ld.LoadAumphur(cbbaumphur, Prov);

            
        }
    }
}
