﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace sdsystem
{
    public partial class FrmRegister44 : Form
    {
        public FrmRegister44()
        {
            InitializeComponent();
        }

        private void FrmRegister44_Load(object sender, EventArgs e)
        {
            LibDisplay ld = new LibDisplay();
            ld.GetDay(cbbDay);
            ld.GetMonth(cbbmonth);
            ld.GetYear(cbbyear);

            ld.GetDay(cbbdayby);
            ld.GetMonth(cbbmonthby);
            ld.GetYear(cbbyearby);

            ld.GetDay(cbbdate_birth);
            ld.GetMonth(cbbmonth_birth);
            ld.GetYearRegis(cbbyear_birth);
            ld.LoadSDPosition(cbbsdposition);
            ld.LoadScar(cbbscar);
            ld.LoadNationality(cbbnation);
            ld.LoadOccupation(cbbocupation);
            ld.LoadReligion(cbbreligion);
            ld.LoadEducation(cbbeducation);
            ld.LoadRegisFollow(cbbregisfollow);
            ld.LoadNationality(cbbfanationallity);
            ld.LoadOccupation(cbbfaoccupation);
            ld.LoadTumbon(cbbtumbon, LoginInfo.AmphurID);
            ld.GetMoo(cbbmoo);
            // ld.LoadAumphur(cbbaumphur, LoginInfo.ProvinceID);

            txtM18.Enabled = false;
            label35.Text = "";

            this.WindowState = FormWindowState.Maximized;
            panel1.Width = this.Width;
            panel2.Width = this.Width;
            btPrintSd1.Enabled = false;
            btPrintSd35.Enabled = false;
            btPrintSd44.Enabled = false;
            btPrintSd9.Enabled = false;
            btPrintKadee.Enabled = false;
            txtby.Text = "อำเภอ" + LoginInfo.AmphurName;

        }

        private void btNewRegist_Click(object sender, EventArgs e)
        {
            
           

        }

  

        private void btCloseForm_Click(object sender, EventArgs e)
        {
            this.Hide();
            
        }

        private void btReadFromCard_Click(object sender, EventArgs e)

        {

            DateTime MyDate = new DateTime();

            int Year = MyDate.Year;
            int Month = MyDate.Month;
            int Day = MyDate.Day;
            //MessageBox.Show(cbbmonth.SelectedValue.ToString());
            MessageBox.Show(""+Year);

              MessageBox.Show(cbbyear.SelectedValue.ToString());
           
            //MessageBox.Show(""+Y);






        }

        private void cbbyear_birth_SelectedIndexChanged(object sender, EventArgs e)
        {
            LibDisplay ld = new LibDisplay();
            if (cbbyear_birth.SelectedIndex == -1)
            {
                txtname.Text = string.Empty;
            }
            else

            {
                 String year_birth =cbbyear_birth.SelectedItem.ToString();
                String substring = year_birth.Substring(1, 4);
                //  txtAge.Text = substring;
                String year_r = cbbyear.SelectedItem.ToString();
                String year_r_n = year_r.Substring(1, 4);
                int YearRegis = Int32.Parse(year_r_n);
                int YearBirth = Int32.Parse(substring);
                int Age = YearRegis - YearBirth;
                if (Age <= 16 || Age >= 30)
                {
                    MessageBox.Show("บุคคลที่จะต้องลงบัญชีทหารกองเกินต้องมีอายุระหว่าง 17-29 ปี", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtAge.Text = "";
                    label35.Text = "";
                    txtclass.Text = "";
                }
                else
                {
                    if (Age >= 18 && Age <=19)
                    {
                        txtM18.Enabled = true;
                        label35.Text = "แสดงตนฯ ตามมาตรา 18";
                        label35.ForeColor = Color.Red;
                    }
                    else if(Age >= 20)
                    {
                        txtM18.Enabled = true;
                        label35.Text = "แสดงตนฯ ตามมาตรา 18 และต้องรับหมายเรียกฯ";
                        label35.ForeColor = Color.Red;
                    }
                    else
                    {
                        txtM18.Enabled = false;
                        label35.Text = "แสดงตนฯ ตามมาตรา 16";
                        label35.ForeColor = Color.GreenYellow;
                    }
                    txtAge.Text = "" + Age;
                    txtZodiac.Text = ld.GetThaiZodiac(YearBirth);

                }

                int classnumber = YearBirth + 18;
                txtclass.Text = ""+classnumber;




            }
        }

        private void cbbyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbyear_birth.SelectedIndex == -1)
            {
                txtname.Text = string.Empty;
            }
            else

            {
                String year_birth = cbbyear_birth.SelectedItem.ToString();
                String substring = year_birth.Substring(1, 4);
                //  txtAge.Text = substring;
                String year_r = cbbyear.SelectedItem.ToString();
                String year_r_n = year_r.Substring(1, 4);
                int YearRegis = Int32.Parse(year_r_n);
                int YearBirth = Int32.Parse(substring);
                int Age = YearRegis - YearBirth;
                if (Age <= 16 || Age >= 30)
                {
                    MessageBox.Show("บุคคลที่จะต้องลงบัญชีทหารกองเกินต้องมีอายุระหว่าง 17-29 ปี", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtAge.Text = "";
                    label35.Text = "";
                }
                else
                {
                    if(Age>=18)
                    {
                        txtM18.Enabled = true;
                        label35.Text = "แสดงตนฯ ตามมาตรา 18";
                        label35.ForeColor = Color.Red;
                    }
                    else
                    {
                        txtM18.Enabled = false;
                        label35.Text = "แสดงตนฯ ตามมาตรา 16";
                        label35.ForeColor = Color.GreenYellow;
                    }
                    txtAge.Text = "" + Age;
                }


            }

        }

        private void txtlastname_TextChanged(object sender, EventArgs e)
        {
            txt_falname.Text = txtlastname.Text;
            txt_malname.Text = txtlastname.Text;

        }

        private void cbbmoo_SelectedIndexChanged(object sender, EventArgs e)
        {
            LibDisplay ld = new LibDisplay();
            string moo = cbbmoo.SelectedValue.ToString();
            string tumbon = cbbtumbon.SelectedValue.ToString();
            ld.GetMooBanName(txtban, moo,tumbon);
        }

        private void txtnumber_TextChanged(object sender, EventArgs e)
        {
            LibDisplay ld = new LibDisplay();
            ld.GetBookNumber(txtbooknumber, txtnumber.Text);
        }

        private void txtPID_KeyPress(object sender, MaskInputRejectedEventArgs e)
        {
            String username = txtPID.Text;
            username = username.Replace("-", "");
            
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            LibSQL Sql = new LibSQL();

            string dor = cbbDay.SelectedValue.ToString();
            string mor = cbbmonth.SelectedValue.ToString();
            string yor = cbbyear.SelectedValue.ToString();
            string pid = txtPID.Text;
            string name = txtname.Text;
            string lastname = txtlastname.Text;
            string dob = cbbdate_birth.SelectedValue.ToString();
            string mob = cbbmonth_birth.SelectedValue.ToString();
            string yob = cbbyear_birth.SelectedValue.ToString();
            string age = txtAge.Text;
            string zodiac = txtZodiac.Text;
            string scar = cbbscar.SelectedValue.ToString();
            string nationality = cbbnation.SelectedValue.ToString();
            string riligion = cbbreligion.SelectedValue.ToString();
            string education = cbbeducation.SelectedValue.ToString();
            string occupation = cbbocupation.SelectedValue.ToString();
            string regisfollow = cbbregisfollow.SelectedValue.ToString();
            string fa_name = txtfaname.Text;
            string fa_lname = txt_falname.Text;
            string fa_nationality = cbbfanationallity.SelectedValue.ToString();
            string fa_occupation = cbbfaoccupation.SelectedValue.ToString();
            string ma_name = txtmaname.Text;
            string ma_lname = txt_malname.Text;
            string banid = txtbanid.Text;
            string moo = cbbmoo.SelectedValue.ToString();
            string banname = txtban.Text;
            string road = txtroad.Text;
            string trok = txttrok.Text;
            string soy = txtsoy.Text;
            string tumbon = cbbtumbon.SelectedValue.ToString();
            string aumphur = LoginInfo.AmphurID;
            string province = LoginInfo.ProvinceID;
            string telephone = txttel.Text;
            string sd1number = txtnumber.Text;
            string sd1booknumber = txtbooknumber.Text;
            string classyear = txtclass.Text;
            string cardid = txtcardid.Text;
            cardid = cardid.Replace("-", "");
            string datecard = cbbdayby.SelectedValue.ToString();
            string monthcard = cbbmonthby.SelectedValue.ToString();
            string yearcard = cbbyearby.SelectedValue.ToString();
            string cardfrom = txtby.Text;
            string regisby = cbbsdposition.SelectedValue.ToString();
            string m18 = txtM18.Text;
            string addby = LoginInfo.UserId;
            string adddatetime = DateTime.Now.ToString("d MMMM yyyy H:mm:ss ");



            if (dor == "0" || mor == "0" || yor == "0" || pid == "" || name == "" || lastname == "" || dob == "0" || mob == "0" || yob == "0" || age == "" || zodiac == "" || scar == "0" || nationality == "0" || riligion == "0" || education == "0" || occupation == "0" || regisfollow == "0" || fa_name == ""
                || fa_lname == "" || fa_nationality == "0" || fa_occupation == "0" || ma_name == "" || ma_lname == "" || banid == "" || moo == "0" || banname == ""
                 || road == "" || trok == "" || soy == "" || tumbon == "0" || telephone == "" || sd1number == "" || sd1booknumber == "" || classyear == ""
                 || cardid == "" || datecard == "0" || monthcard == "0" || yearcard == "0" || cardfrom == "" || regisby == "0" )
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบทุกช่องด้วย!", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {

                string sqlcheck = "select pid from tb_datasd where pid ='" + pid + "'";
                Sql.checkNumrows(sqlcheck, 1);
                string StrQuery = "insert into tb_datasd (dor,mor,yor,pid,name,lastname,dob,mob,yob,age,zodiac,scar,nationality," +
                    "riligion,education,occupation,regisfollow,fa_name,fa_lastname,fa_nationality,fa_occupation,ma_name,ma_lastname," +
                    "banid,moo,banname,road,trok,soy,tumbon,aumphur,province,telephone,sd1number,sd1booknumber,classyear,cardid,datecard," +
                    "monthcard,yearcard,cardfrom,regisby,m18,addby,adddatetime)" +
                    " values('" + dor + "','" + mor + "','" + yor + "','" + pid + "','" + name + "','" + lastname + "','" + dob + "','" + mob + "','" + yob + "','" + age + "'," +
                    "'" + zodiac + "','" + scar + "','" + nationality + "','" + riligion + "','" + education + "','" + occupation + "','" + regisfollow + "','" + fa_name + "'," +
                    "'" + fa_lname + "','" + fa_nationality + "','" + fa_occupation + "','" + ma_name + "','" + ma_lname + "','" + banid + "','" + moo + "','" + banname + "'," +
                    "'" + road + "','" + trok + "','" + soy + "','" + tumbon + "','" + aumphur + "','" + province + "','" + telephone + "','" + sd1number + "','" + sd1booknumber + "','" + classyear + "'," +
                    "'" + cardid + "','" + datecard + "','" + monthcard + "','" + yearcard + "','" + cardfrom + "','" + regisby + "','" + m18 + "','" + addby + "','" + adddatetime + "')";
                //   Sql.RunQuery(StrQuery, 1); //สั่งบันทึกข้อมูล
                btPrintSd1.Enabled = true;

                btPrintSd44.Enabled = true;
                btPrintSd9.Enabled = true;
                int Newage = Int32.Parse(age);
                if (Newage >= 18 && Newage < 20)
                {
                    btPrintKadee.Enabled = true;
                    btPrintSd35.Enabled = false;
                }
                else if (Newage >= 20)
                {
                    btPrintKadee.Enabled = true;
                    btPrintSd35.Enabled = true;
                }
            }
           

        }

        private void btNewRegist_Click_1(object sender, EventArgs e)
        {
            cbbDay.ResetText();
            cbbmonth.ResetText();
            cbbyear.ResetText();
            txtPID.Text="";
            
            txtname.Text = "";
            txtlastname.Text = "";
            cbbdate_birth.ResetText();
            cbbmonth_birth.ResetText();
            cbbyear_birth.ResetText();
            txtAge.Text = "";
            txtZodiac.Text = "";
            cbbscar.ResetText();
            cbbnation.ResetText();
            cbbreligion.ResetText();
            cbbeducation.ResetText();
            cbbocupation.ResetText();
            cbbregisfollow.ResetText();
            txtfaname.Text = "";
            txt_falname.Text = "";
            cbbfanationallity.ResetText();
            cbbfaoccupation.ResetText();
            txtmaname.Text = "";
            txt_malname.Text = "";
            txtbanid.Text = "";
            cbbmoo.ResetText();
            txtban.Text = "";
            txtroad.Text = "-";
            txttrok.Text = "-";
            txtsoy.Text = "-";
            cbbtumbon.ResetText();

            txttel.Text = "";
            txtnumber.Text = "";
            txtbooknumber.Text = "";
            txtclass.Text = "";
            txtcardid.Text = "";

            cbbdayby.ResetText();
            cbbmonthby.ResetText();
            cbbyearby.ResetText();
            txtby.Text="อำเภอ"+LoginInfo.AmphurName;
            cbbsdposition.ResetText();
            txtM18.Enabled = false;

            btPrintSd1.Enabled = false;
            btPrintSd35.Enabled = false;
            btPrintSd44.Enabled = false;
            btPrintSd9.Enabled = false;
            btPrintKadee.Enabled = false;
           

        }
    }
}
