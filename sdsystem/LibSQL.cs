﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace sdsystem
{
    class LibSQL
    {
        public MySqlConnection connection = null;
        private string server;
        private string database;
        private string uid;
        private string password;
        string connectionString;
        public MySqlCommand Command;
    
        //Constructor
        public LibSQL()
        {
            InitializeDB();
        }

        //Initialize values
        public void InitializeDB()
        {
            server = "127.0.0.1";
            uid = "root";
            password = "1234";
            database = "dbsdsystem";
            
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        public bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
                //MessageBox.Show("OK");
            }
            catch (MySqlException ex)
            {

                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("ไม่สามารถเชื่อมต่อฐานข้อมูลได้ กรุณาติดต่อผู้ดูแลระบบ!", "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Application.Exit();
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }
        public void RunQuery(string query, int process)
        {
            try
            {
                OpenConnection();
                Command = new MySqlCommand(query, connection);
                if (Command.ExecuteNonQuery() == 1)
                {
                    switch (process)
                    {
                        case 1:
                            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            break;
                        case 2:
                            MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            break;
                        case 3:
                            MessageBox.Show("ลบข้อมูลเรียบร้อยแล้ว", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            break;
                    }


                }
                else
                {
                    MessageBox.Show("เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void  checkNumrows(string SqlQuery,int process)
        {
            OpenConnection();
            MySqlCommand cmd = new MySqlCommand(SqlQuery, connection);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                switch(process)
                {
                    case 1: //สำหรับเช็คเลขที่บัตร ปชช.
                        MessageBox.Show("บุคคลนี้ได้แสดงตนเพื่อลงบัญชีทหารแล้ว กรุณาเช็คข้อมูลเพื่อความถูกต้อง", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case 2: //สำหรับเช็คทั่วไป
                        MessageBox.Show("ข้อมูลดังกล่าวมีอยู่ในระบบแล้ว ไม่จำเป็นต้องบันทึกเพิ่ม", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
            }
            else
            {
               // return false;
            }
            
        }
         

    }
}
   
