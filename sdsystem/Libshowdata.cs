﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;
using System.Drawing;

namespace sdsystem
{
    class Libshowdata
    {
        public void LoadDataPerson(DataGridView dtP)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select u.id,u.pid,u.sid,y.yodname,u.name,u.lastname,p.position_name" +
                " from tb_userinfo as u  inner join tb_yod as y on (u.yod = y.id) " +
                "  inner join tb_position as p on (u.position = p.id)" +
                "order by u.id";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            //  DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);

            dtP.DataSource = dt;


            dtP.Columns[0].HeaderText = "ลำดับ";
            dtP.Columns[0].Width = 50;
            dtP.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(35, 68, 146);
            dtP.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

            dtP.EnableHeadersVisualStyles = false;
            dtP.Columns[1].HeaderText = "เลข ปชช.";
            dtP.Columns[1].Width = 120;
            dtP.Columns[2].HeaderText = "เลขประจำตัวทหาร";
            dtP.Columns[2].Width = 120;
            dtP.Columns[3].HeaderText = "ยศ";
            dtP.Columns[3].Width = 50;
            dtP.Columns[4].HeaderText = "ชื่อ";
            dtP.Columns[4].Width = 120;
            dtP.Columns[5].HeaderText = "นามสกุล";
            dtP.Columns[5].Width = 120;
            dtP.Columns[6].HeaderText = "ตำแหน่ง";
            dtP.Columns[6].Width = 150;

        }
        public void LoadDataMooBan(DataGridView dtP)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select u.id,u.moo,u.ban,t.DISTRICT_NAME,a.AMPHUR_NAME,p.PROVINCE_NAME" +
                " from tb_configmooban as u  inner join tb_district as t on (u.tumbon = t.DISTRICT_ID) " +
                "  inner join tb_amphur as a on (u.aumphur = a.AMPHUR_ID)" +
                "  inner join tb_province as p on (u.province = p.PROVINCE_ID)" +
                "order by abs(u.aumphur) asc, abs(u.moo) asc";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            //  DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);

            dtP.DataSource = dt;


            dtP.Columns[0].HeaderText = "ลำดับ";
            dtP.Columns[0].Width = 50;
            dtP.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(35, 68, 146);
            dtP.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

            dtP.EnableHeadersVisualStyles = false;
            dtP.Columns[1].HeaderText = "หมู่ที่";
            dtP.Columns[1].Width = 70;
            dtP.Columns[2].HeaderText = "ชื่อหมู่บ้าน";
            dtP.Columns[2].Width = 150;
            dtP.Columns[3].HeaderText = "ตำบล";
            dtP.Columns[3].Width = 150;
            dtP.Columns[4].HeaderText = "อำเภอ";
            dtP.Columns[4].Width = 150;
            dtP.Columns[5].HeaderText = "จังหวัด";
            dtP.Columns[5].Width = 150;
 

        }
        

        public void LoadDataSD(DataGridView dtP)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select u.id,u.sdname,u.sdarmybook,u.sdgovbook,u.sdbanid,u.sdroad,u.sdsoy,u.sdmoo,t.DISTRICT_NAME," +
                "a.AMPHUR_NAME,p.PROVINCE_NAME,u.sdpostcode,u.sdtel" +
                " from tb_configsd as u  inner join tb_province as p on (u.sdprovince = p.PROVINCE_ID) " +
                "  inner join tb_amphur as a on (u.sdamphur = a.AMPHUR_ID)" +
                "  inner join tb_district  as t on (u.sdtumbon = t.DISTRICT_ID)" +
              //  "  inner join tb_zipcode  as z on (u.sdpostcode = z.ZIPCODE)" +
                "order by u.id";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            //  DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);

            dtP.DataSource = dt;


            dtP.Columns[0].HeaderText = "ลำดับ";
            dtP.Columns[0].Width = 50;
            dtP.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(35, 68, 146);
            dtP.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            

            dtP.EnableHeadersVisualStyles = false;
            dtP.Columns[1].HeaderText = "ชื่อหน่วย";
            dtP.Columns[1].Width = 150;
            dtP.Columns[2].HeaderText = "เลข กห.";
            dtP.Columns[2].Width = 100;
            dtP.Columns[3].HeaderText = "เลข มท.";
            dtP.Columns[3].Width = 100;
            dtP.Columns[4].HeaderText = "เลขที่";
            dtP.Columns[4].Width = 70;
            dtP.Columns[5].HeaderText = "ถนน";
            dtP.Columns[5].Width = 100;
            dtP.Columns[6].HeaderText = "ซอย";
            dtP.Columns[6].Width = 100;
            dtP.Columns[7].HeaderText = "หมู่ที่";
            dtP.Columns[7].Width = 70;
            dtP.Columns[8].HeaderText = "ตำบล";
            dtP.Columns[8].Width = 100;
            dtP.Columns[9].HeaderText = "อำเภอ";
            dtP.Columns[9].Width = 150;
            dtP.Columns[10].HeaderText = "จังหวัด";
            dtP.Columns[10].Width = 150;
            dtP.Columns[11].HeaderText = "รหัสไปรษณีย์";
            dtP.Columns[11].Width = 150;
            dtP.Columns[12].HeaderText = "เบอร์โทร";
            dtP.Columns[12].Width = 150;

        }
    }
}
