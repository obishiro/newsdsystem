﻿namespace sdsystem
{
    partial class FrmRegister44
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtZodiac = new System.Windows.Forms.TextBox();
            this.cbbocupation = new System.Windows.Forms.ComboBox();
            this.cbbeducation = new System.Windows.Forms.ComboBox();
            this.cbbreligion = new System.Windows.Forms.ComboBox();
            this.cbbnation = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbbscar = new System.Windows.Forms.ComboBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.cbbyear_birth = new System.Windows.Forms.ComboBox();
            this.cbbmonth_birth = new System.Windows.Forms.ComboBox();
            this.cbbdate_birth = new System.Windows.Forms.ComboBox();
            this.txtlastname = new System.Windows.Forms.TextBox();
            this.txtname = new System.Windows.Forms.TextBox();
            this.txtPID = new System.Windows.Forms.MaskedTextBox();
            this.cbbyear = new System.Windows.Forms.ComboBox();
            this.cbbmonth = new System.Windows.Forms.ComboBox();
            this.cbbDay = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbbregisfollow = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_malname = new System.Windows.Forms.TextBox();
            this.txtmaname = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbbfaoccupation = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbbfanationallity = new System.Windows.Forms.ComboBox();
            this.txt_falname = new System.Windows.Forms.TextBox();
            this.txtfaname = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtban = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txttel = new System.Windows.Forms.MaskedTextBox();
            this.txtsoy = new System.Windows.Forms.TextBox();
            this.txttrok = new System.Windows.Forms.TextBox();
            this.txtroad = new System.Windows.Forms.TextBox();
            this.cbbmoo = new System.Windows.Forms.ComboBox();
            this.txtbanid = new System.Windows.Forms.TextBox();
            this.cbbtumbon = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtclass = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtbooknumber = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtnumber = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtby = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.cbbyearby = new System.Windows.Forms.ComboBox();
            this.cbbmonthby = new System.Windows.Forms.ComboBox();
            this.cbbdayby = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtcardid = new System.Windows.Forms.MaskedTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtM18 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.cbbsdposition = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btSave = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btPrintKadee = new System.Windows.Forms.Button();
            this.btPrintSd35 = new System.Windows.Forms.Button();
            this.btPrintSd9 = new System.Windows.Forms.Button();
            this.btPrintSd1 = new System.Windows.Forms.Button();
            this.btPrintSd44 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.btCloseForm = new System.Windows.Forms.Button();
            this.btNewRegist = new System.Windows.Forms.Button();
            this.btReadFromCard = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox1.Controls.Add(this.txtZodiac);
            this.groupBox1.Controls.Add(this.cbbocupation);
            this.groupBox1.Controls.Add(this.cbbeducation);
            this.groupBox1.Controls.Add(this.cbbreligion);
            this.groupBox1.Controls.Add(this.cbbnation);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbbscar);
            this.groupBox1.Controls.Add(this.txtAge);
            this.groupBox1.Controls.Add(this.cbbyear_birth);
            this.groupBox1.Controls.Add(this.cbbmonth_birth);
            this.groupBox1.Controls.Add(this.cbbdate_birth);
            this.groupBox1.Controls.Add(this.txtlastname);
            this.groupBox1.Controls.Add(this.txtname);
            this.groupBox1.Controls.Add(this.txtPID);
            this.groupBox1.Controls.Add(this.cbbyear);
            this.groupBox1.Controls.Add(this.cbbmonth);
            this.groupBox1.Controls.Add(this.cbbDay);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(311, 352);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ประวัติทหารกองเกิน";
            // 
            // txtZodiac
            // 
            this.txtZodiac.Location = new System.Drawing.Point(85, 165);
            this.txtZodiac.Name = "txtZodiac";
            this.txtZodiac.Size = new System.Drawing.Size(216, 20);
            this.txtZodiac.TabIndex = 27;
            // 
            // cbbocupation
            // 
            this.cbbocupation.FormattingEnabled = true;
            this.cbbocupation.Location = new System.Drawing.Point(85, 302);
            this.cbbocupation.Name = "cbbocupation";
            this.cbbocupation.Size = new System.Drawing.Size(215, 21);
            this.cbbocupation.TabIndex = 26;
            // 
            // cbbeducation
            // 
            this.cbbeducation.FormattingEnabled = true;
            this.cbbeducation.Location = new System.Drawing.Point(85, 274);
            this.cbbeducation.Name = "cbbeducation";
            this.cbbeducation.Size = new System.Drawing.Size(215, 21);
            this.cbbeducation.TabIndex = 25;
            // 
            // cbbreligion
            // 
            this.cbbreligion.FormattingEnabled = true;
            this.cbbreligion.Location = new System.Drawing.Point(85, 247);
            this.cbbreligion.Name = "cbbreligion";
            this.cbbreligion.Size = new System.Drawing.Size(216, 21);
            this.cbbreligion.TabIndex = 24;
            // 
            // cbbnation
            // 
            this.cbbnation.FormattingEnabled = true;
            this.cbbnation.Location = new System.Drawing.Point(85, 220);
            this.cbbnation.Name = "cbbnation";
            this.cbbnation.Size = new System.Drawing.Size(216, 21);
            this.cbbnation.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(39, 307);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "อาชีพ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 282);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "การศึกษา";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 255);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "ศาสนา";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 228);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "สัญชาติ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "ตำหนิ/แผลเป็น";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "ปีนักษัตร";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "อายุ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "ว ด ปี.เกิด";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "ชื่อ-นามสกุล";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "เลข ปชช.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "วันที่ลงบัญชี";
            // 
            // cbbscar
            // 
            this.cbbscar.FormattingEnabled = true;
            this.cbbscar.Location = new System.Drawing.Point(85, 192);
            this.cbbscar.Name = "cbbscar";
            this.cbbscar.Size = new System.Drawing.Size(216, 21);
            this.cbbscar.TabIndex = 11;
            // 
            // txtAge
            // 
            this.txtAge.Location = new System.Drawing.Point(85, 138);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(216, 20);
            this.txtAge.TabIndex = 9;
            // 
            // cbbyear_birth
            // 
            this.cbbyear_birth.FormattingEnabled = true;
            this.cbbyear_birth.Location = new System.Drawing.Point(234, 110);
            this.cbbyear_birth.Name = "cbbyear_birth";
            this.cbbyear_birth.Size = new System.Drawing.Size(67, 21);
            this.cbbyear_birth.TabIndex = 8;
            this.cbbyear_birth.SelectedIndexChanged += new System.EventHandler(this.cbbyear_birth_SelectedIndexChanged);
            // 
            // cbbmonth_birth
            // 
            this.cbbmonth_birth.FormattingEnabled = true;
            this.cbbmonth_birth.Location = new System.Drawing.Point(128, 110);
            this.cbbmonth_birth.Name = "cbbmonth_birth";
            this.cbbmonth_birth.Size = new System.Drawing.Size(99, 21);
            this.cbbmonth_birth.TabIndex = 7;
            // 
            // cbbdate_birth
            // 
            this.cbbdate_birth.FormattingEnabled = true;
            this.cbbdate_birth.Location = new System.Drawing.Point(85, 110);
            this.cbbdate_birth.Name = "cbbdate_birth";
            this.cbbdate_birth.Size = new System.Drawing.Size(36, 21);
            this.cbbdate_birth.TabIndex = 6;
            // 
            // txtlastname
            // 
            this.txtlastname.Location = new System.Drawing.Point(192, 83);
            this.txtlastname.Name = "txtlastname";
            this.txtlastname.Size = new System.Drawing.Size(109, 20);
            this.txtlastname.TabIndex = 5;
            this.txtlastname.TextChanged += new System.EventHandler(this.txtlastname_TextChanged);
            // 
            // txtname
            // 
            this.txtname.Location = new System.Drawing.Point(85, 83);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(100, 20);
            this.txtname.TabIndex = 4;
            // 
            // txtPID
            // 
            this.txtPID.Location = new System.Drawing.Point(85, 56);
            this.txtPID.Mask = "0-0000-00000-00-0";
            this.txtPID.Name = "txtPID";
            this.txtPID.Size = new System.Drawing.Size(216, 20);
            this.txtPID.TabIndex = 3;
            this.txtPID.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtPID_KeyPress);
            // 
            // cbbyear
            // 
            this.cbbyear.FormattingEnabled = true;
            this.cbbyear.Location = new System.Drawing.Point(234, 28);
            this.cbbyear.Name = "cbbyear";
            this.cbbyear.Size = new System.Drawing.Size(67, 21);
            this.cbbyear.TabIndex = 2;
            this.cbbyear.SelectedIndexChanged += new System.EventHandler(this.cbbyear_SelectedIndexChanged);
            // 
            // cbbmonth
            // 
            this.cbbmonth.FormattingEnabled = true;
            this.cbbmonth.Location = new System.Drawing.Point(128, 28);
            this.cbbmonth.Name = "cbbmonth";
            this.cbbmonth.Size = new System.Drawing.Size(99, 21);
            this.cbbmonth.TabIndex = 1;
            // 
            // cbbDay
            // 
            this.cbbDay.FormattingEnabled = true;
            this.cbbDay.Location = new System.Drawing.Point(85, 28);
            this.cbbDay.Name = "cbbDay";
            this.cbbDay.Size = new System.Drawing.Size(36, 21);
            this.cbbDay.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cbbregisfollow);
            this.groupBox2.Location = new System.Drawing.Point(12, 433);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(311, 60);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ลงบัญชีตามภูมิลำเนา";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "ตามภูมิลำเนา";
            // 
            // cbbregisfollow
            // 
            this.cbbregisfollow.FormattingEnabled = true;
            this.cbbregisfollow.Location = new System.Drawing.Point(87, 19);
            this.cbbregisfollow.Name = "cbbregisfollow";
            this.cbbregisfollow.Size = new System.Drawing.Size(215, 21);
            this.cbbregisfollow.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox3.Controls.Add(this.txt_malname);
            this.groupBox3.Controls.Add(this.txtmaname);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.cbbfaoccupation);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.cbbfanationallity);
            this.groupBox3.Controls.Add(this.txt_falname);
            this.groupBox3.Controls.Add(this.txtfaname);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Location = new System.Drawing.Point(330, 75);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(309, 157);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ข้อมูลบิดา/มารดา";
            // 
            // txt_malname
            // 
            this.txt_malname.Location = new System.Drawing.Point(205, 110);
            this.txt_malname.Name = "txt_malname";
            this.txt_malname.Size = new System.Drawing.Size(98, 20);
            this.txt_malname.TabIndex = 36;
            // 
            // txtmaname
            // 
            this.txtmaname.Location = new System.Drawing.Point(99, 110);
            this.txtmaname.Name = "txtmaname";
            this.txtmaname.Size = new System.Drawing.Size(99, 20);
            this.txtmaname.TabIndex = 35;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 117);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 13);
            this.label16.TabIndex = 34;
            this.label16.Text = "ชื่อ-นามสกุลมารดา";
            // 
            // cbbfaoccupation
            // 
            this.cbbfaoccupation.FormattingEnabled = true;
            this.cbbfaoccupation.Location = new System.Drawing.Point(99, 83);
            this.cbbfaoccupation.Name = "cbbfaoccupation";
            this.cbbfaoccupation.Size = new System.Drawing.Size(204, 21);
            this.cbbfaoccupation.TabIndex = 33;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(41, 91);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "อาชีพบิดา";
            // 
            // cbbfanationallity
            // 
            this.cbbfanationallity.FormattingEnabled = true;
            this.cbbfanationallity.Location = new System.Drawing.Point(99, 55);
            this.cbbfanationallity.Name = "cbbfanationallity";
            this.cbbfanationallity.Size = new System.Drawing.Size(204, 21);
            this.cbbfanationallity.TabIndex = 31;
            // 
            // txt_falname
            // 
            this.txt_falname.Location = new System.Drawing.Point(205, 27);
            this.txt_falname.Name = "txt_falname";
            this.txt_falname.Size = new System.Drawing.Size(98, 20);
            this.txt_falname.TabIndex = 30;
            // 
            // txtfaname
            // 
            this.txtfaname.Location = new System.Drawing.Point(99, 27);
            this.txtfaname.Name = "txtfaname";
            this.txtfaname.Size = new System.Drawing.Size(99, 20);
            this.txtfaname.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(33, 63);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "สัญชาติบิดา";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "ชื่อ-นามสกุลบิดา";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox4.Controls.Add(this.txtban);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.txttel);
            this.groupBox4.Controls.Add(this.txtsoy);
            this.groupBox4.Controls.Add(this.txttrok);
            this.groupBox4.Controls.Add(this.txtroad);
            this.groupBox4.Controls.Add(this.cbbmoo);
            this.groupBox4.Controls.Add(this.txtbanid);
            this.groupBox4.Controls.Add(this.cbbtumbon);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Location = new System.Drawing.Point(329, 239);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(310, 254);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ข้อมูลภูมิลำเนาทหาร";
            // 
            // txtban
            // 
            this.txtban.Location = new System.Drawing.Point(101, 101);
            this.txtban.Name = "txtban";
            this.txtban.Size = new System.Drawing.Size(204, 20);
            this.txtban.TabIndex = 53;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(22, 213);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(72, 13);
            this.label26.TabIndex = 52;
            this.label26.Text = "เบอร์โทรศัพท์";
            // 
            // txttel
            // 
            this.txttel.Location = new System.Drawing.Point(100, 208);
            this.txttel.Mask = "000-000-0000";
            this.txttel.Name = "txttel";
            this.txttel.Size = new System.Drawing.Size(204, 20);
            this.txttel.TabIndex = 51;
            // 
            // txtsoy
            // 
            this.txtsoy.Location = new System.Drawing.Point(100, 182);
            this.txtsoy.Name = "txtsoy";
            this.txtsoy.Size = new System.Drawing.Size(204, 20);
            this.txtsoy.TabIndex = 46;
            this.txtsoy.Text = "-";
            // 
            // txttrok
            // 
            this.txttrok.Location = new System.Drawing.Point(100, 155);
            this.txttrok.Name = "txttrok";
            this.txttrok.Size = new System.Drawing.Size(204, 20);
            this.txttrok.TabIndex = 45;
            this.txttrok.Text = "-";
            // 
            // txtroad
            // 
            this.txtroad.Location = new System.Drawing.Point(100, 128);
            this.txtroad.Name = "txtroad";
            this.txtroad.Size = new System.Drawing.Size(204, 20);
            this.txtroad.TabIndex = 44;
            this.txtroad.Text = "-";
            // 
            // cbbmoo
            // 
            this.cbbmoo.FormattingEnabled = true;
            this.cbbmoo.Location = new System.Drawing.Point(100, 72);
            this.cbbmoo.Name = "cbbmoo";
            this.cbbmoo.Size = new System.Drawing.Size(204, 21);
            this.cbbmoo.TabIndex = 42;
            this.cbbmoo.SelectedIndexChanged += new System.EventHandler(this.cbbmoo_SelectedIndexChanged);
            // 
            // txtbanid
            // 
            this.txtbanid.Location = new System.Drawing.Point(100, 45);
            this.txtbanid.Name = "txtbanid";
            this.txtbanid.Size = new System.Drawing.Size(204, 20);
            this.txtbanid.TabIndex = 41;
            // 
            // cbbtumbon
            // 
            this.cbbtumbon.FormattingEnabled = true;
            this.cbbtumbon.Location = new System.Drawing.Point(100, 17);
            this.cbbtumbon.Name = "cbbtumbon";
            this.cbbtumbon.Size = new System.Drawing.Size(204, 21);
            this.cbbtumbon.TabIndex = 40;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(62, 24);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(32, 13);
            this.label23.TabIndex = 39;
            this.label23.Text = "ตำบล";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(67, 189);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 13);
            this.label22.TabIndex = 38;
            this.label22.Text = "ซอย";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(61, 162);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 36;
            this.label21.Text = "ตรอก";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(64, 134);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 13);
            this.label20.TabIndex = 34;
            this.label20.Text = "ถนน";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(40, 108);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "ชื่อหมู่บ้าน";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(66, 80);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(28, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "หมู่ที่";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(43, 52);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "บ้านเลขที่";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox5.Controls.Add(this.txtclass);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.txtbooknumber);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.txtnumber);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Location = new System.Drawing.Point(646, 75);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(275, 111);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ใบสำคัญ (แบบ สด.9)";
            // 
            // txtclass
            // 
            this.txtclass.Location = new System.Drawing.Point(65, 80);
            this.txtclass.Name = "txtclass";
            this.txtclass.Size = new System.Drawing.Size(204, 20);
            this.txtclass.TabIndex = 5;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(30, 87);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 13);
            this.label29.TabIndex = 4;
            this.label29.Text = "ชั้นปี";
            // 
            // txtbooknumber
            // 
            this.txtbooknumber.Location = new System.Drawing.Point(65, 53);
            this.txtbooknumber.Name = "txtbooknumber";
            this.txtbooknumber.Size = new System.Drawing.Size(204, 20);
            this.txtbooknumber.TabIndex = 3;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(27, 60);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(32, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "เล่มที่";
            // 
            // txtnumber
            // 
            this.txtnumber.Location = new System.Drawing.Point(65, 27);
            this.txtnumber.Name = "txtnumber";
            this.txtnumber.Size = new System.Drawing.Size(204, 20);
            this.txtnumber.TabIndex = 1;
            this.txtnumber.TextChanged += new System.EventHandler(this.txtnumber_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(28, 34);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(31, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "เลขที่";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox6.Controls.Add(this.txtby);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.cbbyearby);
            this.groupBox6.Controls.Add(this.cbbmonthby);
            this.groupBox6.Controls.Add(this.cbbdayby);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.txtcardid);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Location = new System.Drawing.Point(646, 192);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(275, 112);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "ข้อมูลบัตรประจำตัวประชาชน";
            // 
            // txtby
            // 
            this.txtby.Location = new System.Drawing.Point(65, 77);
            this.txtby.Name = "txtby";
            this.txtby.Size = new System.Drawing.Size(204, 20);
            this.txtby.TabIndex = 7;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(14, 84);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(45, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "ออกโดย";
            // 
            // cbbyearby
            // 
            this.cbbyearby.FormattingEnabled = true;
            this.cbbyearby.Location = new System.Drawing.Point(210, 49);
            this.cbbyearby.Name = "cbbyearby";
            this.cbbyearby.Size = new System.Drawing.Size(59, 21);
            this.cbbyearby.TabIndex = 5;
            // 
            // cbbmonthby
            // 
            this.cbbmonthby.FormattingEnabled = true;
            this.cbbmonthby.Location = new System.Drawing.Point(111, 49);
            this.cbbmonthby.Name = "cbbmonthby";
            this.cbbmonthby.Size = new System.Drawing.Size(93, 21);
            this.cbbmonthby.TabIndex = 4;
            // 
            // cbbdayby
            // 
            this.cbbdayby.FormattingEnabled = true;
            this.cbbdayby.Location = new System.Drawing.Point(65, 49);
            this.cbbdayby.Name = "cbbdayby";
            this.cbbdayby.Size = new System.Drawing.Size(40, 21);
            this.cbbdayby.TabIndex = 3;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(13, 57);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(46, 13);
            this.label31.TabIndex = 2;
            this.label31.Text = "เมื่อวันที่";
            // 
            // txtcardid
            // 
            this.txtcardid.Location = new System.Drawing.Point(65, 24);
            this.txtcardid.Mask = "0000-00-00000000";
            this.txtcardid.Name = "txtcardid";
            this.txtcardid.Size = new System.Drawing.Size(204, 20);
            this.txtcardid.TabIndex = 1;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(8, 32);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "เลขที่บัตร";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox7.Controls.Add(this.txtM18);
            this.groupBox7.Controls.Add(this.label34);
            this.groupBox7.Controls.Add(this.cbbsdposition);
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Location = new System.Drawing.Point(646, 311);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(275, 86);
            this.groupBox7.TabIndex = 11;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "ข้อมูลผู้รับลงบัญชี/ที่คดี";
            // 
            // txtM18
            // 
            this.txtM18.Location = new System.Drawing.Point(71, 50);
            this.txtM18.Name = "txtM18";
            this.txtM18.Size = new System.Drawing.Size(198, 20);
            this.txtM18.TabIndex = 10;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(14, 57);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 13);
            this.label34.TabIndex = 9;
            this.label34.Text = "ที่คดี ม.18";
            // 
            // cbbsdposition
            // 
            this.cbbsdposition.FormattingEnabled = true;
            this.cbbsdposition.Location = new System.Drawing.Point(71, 22);
            this.cbbsdposition.Name = "cbbsdposition";
            this.cbbsdposition.Size = new System.Drawing.Size(198, 21);
            this.cbbsdposition.TabIndex = 8;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(14, 31);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(50, 13);
            this.label33.TabIndex = 7;
            this.label33.Text = "เจ้าหน้าที่";
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.RoyalBlue;
            this.groupBox8.Controls.Add(this.btSave);
            this.groupBox8.ForeColor = System.Drawing.Color.White;
            this.groupBox8.Location = new System.Drawing.Point(646, 401);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(275, 92);
            this.groupBox8.TabIndex = 12;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "แถบเครื่องมือ";
            // 
            // btSave
            // 
            this.btSave.BackColor = System.Drawing.Color.DarkGreen;
            this.btSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btSave.ForeColor = System.Drawing.Color.White;
            this.btSave.Image = global::sdsystem.Properties.Resources.save_all_icon;
            this.btSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btSave.Location = new System.Drawing.Point(7, 20);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(262, 61);
            this.btSave.TabIndex = 0;
            this.btSave.Text = "บันทึกข้อมูล";
            this.btSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btSave.UseVisualStyleBackColor = false;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(68)))), ((int)(((byte)(146)))));
            this.panel2.Controls.Add(this.btPrintKadee);
            this.panel2.Controls.Add(this.btPrintSd35);
            this.panel2.Controls.Add(this.btPrintSd9);
            this.panel2.Controls.Add(this.btPrintSd1);
            this.panel2.Controls.Add(this.btPrintSd44);
            this.panel2.Location = new System.Drawing.Point(0, 499);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(931, 83);
            this.panel2.TabIndex = 4;
            // 
            // btPrintKadee
            // 
            this.btPrintKadee.BackColor = System.Drawing.Color.Red;
            this.btPrintKadee.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btPrintKadee.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btPrintKadee.Image = global::sdsystem.Properties.Resources.print_icon;
            this.btPrintKadee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrintKadee.Location = new System.Drawing.Point(595, 22);
            this.btPrintKadee.Name = "btPrintKadee";
            this.btPrintKadee.Size = new System.Drawing.Size(174, 49);
            this.btPrintKadee.TabIndex = 13;
            this.btPrintKadee.Text = "พิมพ์ ส่งตัวดำเนินคดี";
            this.btPrintKadee.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPrintKadee.UseVisualStyleBackColor = false;
            // 
            // btPrintSd35
            // 
            this.btPrintSd35.BackColor = System.Drawing.Color.Yellow;
            this.btPrintSd35.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btPrintSd35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btPrintSd35.Image = global::sdsystem.Properties.Resources.print_icon;
            this.btPrintSd35.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrintSd35.Location = new System.Drawing.Point(425, 22);
            this.btPrintSd35.Name = "btPrintSd35";
            this.btPrintSd35.Size = new System.Drawing.Size(152, 49);
            this.btPrintSd35.TabIndex = 3;
            this.btPrintSd35.Text = "พิมพ์หมายเรียกฯ";
            this.btPrintSd35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPrintSd35.UseVisualStyleBackColor = false;
            // 
            // btPrintSd9
            // 
            this.btPrintSd9.BackColor = System.Drawing.Color.Aqua;
            this.btPrintSd9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btPrintSd9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btPrintSd9.Image = global::sdsystem.Properties.Resources.print_icon;
            this.btPrintSd9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrintSd9.Location = new System.Drawing.Point(290, 22);
            this.btPrintSd9.Name = "btPrintSd9";
            this.btPrintSd9.Size = new System.Drawing.Size(116, 49);
            this.btPrintSd9.TabIndex = 2;
            this.btPrintSd9.Text = "พิมพ์ สด.9";
            this.btPrintSd9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPrintSd9.UseVisualStyleBackColor = false;
            // 
            // btPrintSd1
            // 
            this.btPrintSd1.BackColor = System.Drawing.Color.Lime;
            this.btPrintSd1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btPrintSd1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btPrintSd1.Image = global::sdsystem.Properties.Resources.print_icon;
            this.btPrintSd1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrintSd1.Location = new System.Drawing.Point(156, 22);
            this.btPrintSd1.Name = "btPrintSd1";
            this.btPrintSd1.Size = new System.Drawing.Size(116, 49);
            this.btPrintSd1.TabIndex = 1;
            this.btPrintSd1.Text = "พิมพ์ สด.1";
            this.btPrintSd1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPrintSd1.UseVisualStyleBackColor = false;
            // 
            // btPrintSd44
            // 
            this.btPrintSd44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btPrintSd44.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btPrintSd44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btPrintSd44.Image = global::sdsystem.Properties.Resources.print_icon;
            this.btPrintSd44.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrintSd44.Location = new System.Drawing.Point(13, 22);
            this.btPrintSd44.Name = "btPrintSd44";
            this.btPrintSd44.Size = new System.Drawing.Size(125, 49);
            this.btPrintSd44.TabIndex = 0;
            this.btPrintSd44.Text = "พิมพ์ สด.44";
            this.btPrintSd44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPrintSd44.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(68)))), ((int)(((byte)(146)))));
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.btCloseForm);
            this.panel1.Controls.Add(this.btNewRegist);
            this.panel1.Controls.Add(this.btReadFromCard);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(931, 68);
            this.panel1.TabIndex = 3;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.White;
            this.label35.Location = new System.Drawing.Point(423, 15);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(0, 42);
            this.label35.TabIndex = 3;
            // 
            // btCloseForm
            // 
            this.btCloseForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btCloseForm.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btCloseForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btCloseForm.ForeColor = System.Drawing.Color.White;
            this.btCloseForm.Image = global::sdsystem.Properties.Resources.sign_error_icon;
            this.btCloseForm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCloseForm.Location = new System.Drawing.Point(305, 11);
            this.btCloseForm.Name = "btCloseForm";
            this.btCloseForm.Size = new System.Drawing.Size(101, 45);
            this.btCloseForm.TabIndex = 2;
            this.btCloseForm.Text = "ปิดหน้านี้";
            this.btCloseForm.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btCloseForm.UseVisualStyleBackColor = false;
            this.btCloseForm.Click += new System.EventHandler(this.btCloseForm_Click);
            // 
            // btNewRegist
            // 
            this.btNewRegist.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btNewRegist.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btNewRegist.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btNewRegist.ForeColor = System.Drawing.Color.White;
            this.btNewRegist.Image = global::sdsystem.Properties.Resources.user_icon;
            this.btNewRegist.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btNewRegist.Location = new System.Drawing.Point(13, 12);
            this.btNewRegist.Name = "btNewRegist";
            this.btNewRegist.Size = new System.Drawing.Size(116, 45);
            this.btNewRegist.TabIndex = 0;
            this.btNewRegist.Text = "แสดงตนใหม่";
            this.btNewRegist.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btNewRegist.UseVisualStyleBackColor = false;
            this.btNewRegist.Click += new System.EventHandler(this.btNewRegist_Click_1);
            // 
            // btReadFromCard
            // 
            this.btReadFromCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btReadFromCard.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btReadFromCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btReadFromCard.ForeColor = System.Drawing.Color.White;
            this.btReadFromCard.Image = global::sdsystem.Properties.Resources.secure_card_icon;
            this.btReadFromCard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btReadFromCard.Location = new System.Drawing.Point(140, 11);
            this.btReadFromCard.Name = "btReadFromCard";
            this.btReadFromCard.Size = new System.Drawing.Size(157, 45);
            this.btReadFromCard.TabIndex = 1;
            this.btReadFromCard.Text = "ดึงข้อมูลจากบัตรฯ";
            this.btReadFromCard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btReadFromCard.UseVisualStyleBackColor = false;
            this.btReadFromCard.Click += new System.EventHandler(this.btReadFromCard_Click);
            // 
            // FrmRegister44
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(931, 587);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmRegister44";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "แสดงตนเพื่อลงบัญชีทหารกองเกิน";
            this.Load += new System.EventHandler(this.FrmRegister44_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btNewRegist;
        private System.Windows.Forms.Button btReadFromCard;
        private System.Windows.Forms.Button btCloseForm;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbbyear_birth;
        private System.Windows.Forms.ComboBox cbbmonth_birth;
        private System.Windows.Forms.ComboBox cbbdate_birth;
        private System.Windows.Forms.TextBox txtlastname;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.MaskedTextBox txtPID;
        private System.Windows.Forms.ComboBox cbbyear;
        private System.Windows.Forms.ComboBox cbbmonth;
        private System.Windows.Forms.ComboBox cbbDay;
        private System.Windows.Forms.ComboBox cbbocupation;
        private System.Windows.Forms.ComboBox cbbeducation;
        private System.Windows.Forms.ComboBox cbbreligion;
        private System.Windows.Forms.ComboBox cbbnation;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbbscar;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbbregisfollow;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txt_malname;
        private System.Windows.Forms.TextBox txtmaname;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbbfaoccupation;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbbfanationallity;
        private System.Windows.Forms.TextBox txt_falname;
        private System.Windows.Forms.TextBox txtfaname;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.MaskedTextBox txttel;
        private System.Windows.Forms.TextBox txtsoy;
        private System.Windows.Forms.TextBox txttrok;
        private System.Windows.Forms.TextBox txtroad;
        private System.Windows.Forms.ComboBox cbbmoo;
        private System.Windows.Forms.TextBox txtbanid;
        private System.Windows.Forms.ComboBox cbbtumbon;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtclass;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtbooknumber;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtnumber;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtby;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cbbyearby;
        private System.Windows.Forms.ComboBox cbbmonthby;
        private System.Windows.Forms.ComboBox cbbdayby;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.MaskedTextBox txtcardid;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtM18;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cbbsdposition;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btPrintKadee;
        private System.Windows.Forms.Button btPrintSd35;
        private System.Windows.Forms.Button btPrintSd9;
        private System.Windows.Forms.Button btPrintSd1;
        private System.Windows.Forms.Button btPrintSd44;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtZodiac;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtban;
    }
}