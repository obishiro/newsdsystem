﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace sdsystem
{
    public partial class FrmHomeName : Form
    {
        public FrmHomeName()
        {
            InitializeComponent();
        }

        private void FrmHomeName_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            panel1.Width = this.Width;
            LibDisplay ld = new LibDisplay();
            Libshowdata libShowData = new Libshowdata();
            ld.LoadTumbon(cbbtumbon, LoginInfo.AmphurID);
            ld.GetMoo(cbbmoo);
            libShowData.LoadDataMooBan(dataMooban);
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            string Tumbon = cbbtumbon.SelectedValue.ToString();
            string Moo = cbbmoo.SelectedValue.ToString();
            string Ban = txtbanname.Text;
            string Aumphur = LoginInfo.AmphurID;
            string Prov = LoginInfo.ProvinceID;
            if (Tumbon == "0" || Moo == "0" || Ban == "")
            {

                MessageBox.Show("กรุณากรอกข้อมูลให้ครบทุกช่องด้วย!", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                LibSQL Connection = new LibSQL();
                Libshowdata libShowData = new Libshowdata();
                Connection.OpenConnection();

                string StrSql = "insert into tb_configmooban (tumbon,moo,ban,aumphur,province) values" +
                    "('" + Tumbon + "','" + Moo + "','" + Ban + "','" + Aumphur + "','" + Prov + "') ";
                MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
                MySqlDataReader reader;
                reader = cmd.ExecuteReader();
                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว!", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                libShowData.LoadDataMooBan(dataMooban);
            }
        }
    }
}
