﻿namespace sdsystem
{
    partial class FrmConfigSD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConfigSD));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txttel = new System.Windows.Forms.TextBox();
            this.txtgovbook = new System.Windows.Forms.TextBox();
            this.txtarmybook = new System.Windows.Forms.TextBox();
            this.txtsdname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtpostcode = new System.Windows.Forms.TextBox();
            this.cbbtumbon = new System.Windows.Forms.ComboBox();
            this.cbbaumphur = new System.Windows.Forms.ComboBox();
            this.cbbprovince = new System.Windows.Forms.ComboBox();
            this.txtmoo = new System.Windows.Forms.TextBox();
            this.txtsoy = new System.Windows.Forms.TextBox();
            this.txtroad = new System.Windows.Forms.TextBox();
            this.txtbanid = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.dataSD = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSD)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox1.Controls.Add(this.txttel);
            this.groupBox1.Controls.Add(this.txtgovbook);
            this.groupBox1.Controls.Add(this.txtarmybook);
            this.groupBox1.Controls.Add(this.txtsdname);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(12, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(289, 150);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ตั้งค่าหน่วยสัสดีอำเภอ";
            // 
            // txttel
            // 
            this.txttel.Location = new System.Drawing.Point(121, 107);
            this.txttel.Name = "txttel";
            this.txttel.Size = new System.Drawing.Size(150, 21);
            this.txttel.TabIndex = 11;
            // 
            // txtgovbook
            // 
            this.txtgovbook.Location = new System.Drawing.Point(121, 79);
            this.txtgovbook.Name = "txtgovbook";
            this.txtgovbook.Size = new System.Drawing.Size(150, 21);
            this.txtgovbook.TabIndex = 10;
            // 
            // txtarmybook
            // 
            this.txtarmybook.Location = new System.Drawing.Point(121, 51);
            this.txtarmybook.Name = "txtarmybook";
            this.txtarmybook.Size = new System.Drawing.Size(150, 21);
            this.txtarmybook.TabIndex = 9;
            // 
            // txtsdname
            // 
            this.txtsdname.Location = new System.Drawing.Point(121, 23);
            this.txtsdname.Name = "txtsdname";
            this.txtsdname.Size = new System.Drawing.Size(150, 21);
            this.txtsdname.TabIndex = 8;
            this.txtsdname.Text = "หน่วยสัสดี";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "เบอร์โทรศัพท์";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "เลขที่หนังสืออำเภอ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "เลขที่หนังสือ กห.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "ชื่อหน่่วยสัสดีอำเภอ";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.txtpostcode);
            this.groupBox2.Controls.Add(this.cbbtumbon);
            this.groupBox2.Controls.Add(this.cbbaumphur);
            this.groupBox2.Controls.Add(this.cbbprovince);
            this.groupBox2.Controls.Add(this.txtmoo);
            this.groupBox2.Controls.Add(this.txtsoy);
            this.groupBox2.Controls.Add(this.txtroad);
            this.groupBox2.Controls.Add(this.txtbanid);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 251);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(289, 310);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ที่ตั้งหน่วยสัสดีอำเภอ";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = global::sdsystem.Properties.Resources.sign_error_icon;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.Location = new System.Drawing.Point(203, 241);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(68, 54);
            this.button2.TabIndex = 33;
            this.button2.Text = "ปิดหน้านี้";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkGreen;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::sdsystem.Properties.Resources.save_all_icon;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.Location = new System.Drawing.Point(121, 241);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 54);
            this.button1.TabIndex = 32;
            this.button1.Text = "บันทึกข้อมูล";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtpostcode
            // 
            this.txtpostcode.Location = new System.Drawing.Point(121, 214);
            this.txtpostcode.Name = "txtpostcode";
            this.txtpostcode.Size = new System.Drawing.Size(150, 20);
            this.txtpostcode.TabIndex = 31;
            // 
            // cbbtumbon
            // 
            this.cbbtumbon.FormattingEnabled = true;
            this.cbbtumbon.Location = new System.Drawing.Point(121, 187);
            this.cbbtumbon.Name = "cbbtumbon";
            this.cbbtumbon.Size = new System.Drawing.Size(150, 21);
            this.cbbtumbon.TabIndex = 30;
            // 
            // cbbaumphur
            // 
            this.cbbaumphur.FormattingEnabled = true;
            this.cbbaumphur.Location = new System.Drawing.Point(121, 160);
            this.cbbaumphur.Name = "cbbaumphur";
            this.cbbaumphur.Size = new System.Drawing.Size(150, 21);
            this.cbbaumphur.TabIndex = 29;
            this.cbbaumphur.SelectedIndexChanged += new System.EventHandler(this.cbbaumphur_SelectedIndexChanged);
            // 
            // cbbprovince
            // 
            this.cbbprovince.FormattingEnabled = true;
            this.cbbprovince.Location = new System.Drawing.Point(121, 133);
            this.cbbprovince.Name = "cbbprovince";
            this.cbbprovince.Size = new System.Drawing.Size(150, 21);
            this.cbbprovince.TabIndex = 28;
            this.cbbprovince.SelectedIndexChanged += new System.EventHandler(this.cbbprovince_SelectedIndexChanged);
            // 
            // txtmoo
            // 
            this.txtmoo.Location = new System.Drawing.Point(121, 106);
            this.txtmoo.Name = "txtmoo";
            this.txtmoo.Size = new System.Drawing.Size(150, 20);
            this.txtmoo.TabIndex = 27;
            // 
            // txtsoy
            // 
            this.txtsoy.Location = new System.Drawing.Point(121, 79);
            this.txtsoy.Name = "txtsoy";
            this.txtsoy.Size = new System.Drawing.Size(150, 20);
            this.txtsoy.TabIndex = 26;
            // 
            // txtroad
            // 
            this.txtroad.Location = new System.Drawing.Point(121, 52);
            this.txtroad.Name = "txtroad";
            this.txtroad.Size = new System.Drawing.Size(150, 20);
            this.txtroad.TabIndex = 25;
            // 
            // txtbanid
            // 
            this.txtbanid.Location = new System.Drawing.Point(121, 25);
            this.txtbanid.Name = "txtbanid";
            this.txtbanid.Size = new System.Drawing.Size(150, 20);
            this.txtbanid.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(40, 221);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "รหัสไปรษณีย์";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(72, 195);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "ตำบล";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(67, 168);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "อำเภอ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(65, 141);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "จังหวัด";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(74, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "หมู่ที่";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "ตรอก/ซอย";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(73, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "ถนน";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(73, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "เลขที่";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(68)))), ((int)(((byte)(146)))));
            this.panel1.Controls.Add(this.label13);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(954, 76);
            this.panel1.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(12, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(422, 37);
            this.label13.TabIndex = 0;
            this.label13.Text = "แบบฟอร์มตั้งค่าหน่วยสัสดีอำเภอ";
            // 
            // dataSD
            // 
            this.dataSD.AllowUserToAddRows = false;
            this.dataSD.AllowUserToDeleteRows = false;
            this.dataSD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataSD.Location = new System.Drawing.Point(6, 22);
            this.dataSD.Name = "dataSD";
            this.dataSD.ReadOnly = true;
            this.dataSD.Size = new System.Drawing.Size(927, 429);
            this.dataSD.TabIndex = 12;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox3.Controls.Add(this.dataSD);
            this.groupBox3.Location = new System.Drawing.Point(308, 95);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(942, 465);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "รายการข้อมูล";
            // 
            // FrmConfigSD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1256, 572);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfigSD";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ตั้งค่าหน่วยสัสดีอำเภอ";
            this.Load += new System.EventHandler(this.FrmConfigSD_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSD)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dataSD;
        private System.Windows.Forms.TextBox txttel;
        private System.Windows.Forms.TextBox txtgovbook;
        private System.Windows.Forms.TextBox txtarmybook;
        private System.Windows.Forms.TextBox txtsdname;
        private System.Windows.Forms.TextBox txtpostcode;
        private System.Windows.Forms.ComboBox cbbtumbon;
        private System.Windows.Forms.ComboBox cbbaumphur;
        private System.Windows.Forms.ComboBox cbbprovince;
        private System.Windows.Forms.TextBox txtmoo;
        private System.Windows.Forms.TextBox txtsoy;
        private System.Windows.Forms.TextBox txtroad;
        private System.Windows.Forms.TextBox txtbanid;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}