﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;







namespace sdsystem
{
    
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
            
            
        }

       

      

     /*   private void btConfig_Click(object sender, EventArgs e)
        {
            var formConfig = new FrmConfig();
            formConfig.Show();


        }*/

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            LibSQL NewConnection = new LibSQL();
             
            NewConnection.OpenConnection();
            textBox1.Focus();
            



        }

      

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบทุกช่อง!", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                String username = textBox1.Text;
                username = username.Replace("-", "");

                //Conn = NewConnection.connection.Open();            
                string SqlQuery = "select u.username,uf.name,uf.lastname,uf.pid,uy.yodname,p.position_name ,prov.PROVINCE_NAME , a.AMPHUR_NAME,prov.PROVINCE_ID , a.AMPHUR_ID from tb_user as u " +
                    "inner join tb_userinfo as uf on (u.username = uf.pid)" +
                    "inner join tb_yod as uy on (uf.yod = uy.id)" +
                    "inner join tb_position as p on (uf.position = p.id)" +
                    "inner join tb_province as prov on (uf.province = prov.PROVINCE_ID)"+
                    "inner join tb_amphur as a on (uf.aumphur = a.AMPHUR_ID)" +
                    " where u.username='" + username + "" +
                    "' and u.password='" + textBox2.Text + "" +
                    "' and u.permission ='1' ";
                //
                LibSQL Connect = new LibSQL();
                Connect.OpenConnection();
                MySqlCommand cmd = new MySqlCommand(SqlQuery, Connect.connection);
                MySqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        MessageBox.Show("ยินดีต้อนรับเข้าสู่ระบบสนับสนุนสายงานสัสดี", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    }
                    string uname = "" + rdr["yodname"] + "" + rdr["name"] + " " + rdr["lastname"];
                    string uposition = "" + rdr["position_name"]+rdr["AMPHUR_NAME"]+"จังหวัด"+rdr["PROVINCE_NAME"];
                    string uid = "" + rdr["pid"];// + " " + rdr["lastname"];
                    LoginInfo.UserId = uid;
                    LoginInfo.UserName = uname;
                    LoginInfo.UserPosition = uposition;
                    LoginInfo.AmphurID = "" + rdr["AMPHUR_ID"];
                    LoginInfo.ProvinceID = ""+rdr["PROVINCE_ID"];
                    LoginInfo.AmphurName = "" + rdr["AMPHUR_NAME"];
                    LoginInfo.ProvinceName = "" + rdr["PROVINCE_NAME"];

                    rdr.Close();

                    this.Visible = false;
                    var Mainform = new MainForm();
                    Mainform.Show();
                }
                else
                {
                    MessageBox.Show("ไม่สามารถเข้าสู่ระบบได้ เนื่องจากชื่อและรหัสผ่านผิด!", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Text = "";
                    textBox1.Focus();
                    textBox2.Text = "";
                }

            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ท่านต้องการออกจากระบบจริงหรือไม่", "ยืนยันออกจากระบบ", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {

            }
        }

        private void textBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
    public static class LoginInfo
    {
        public static  string UserId;
        public static string UserName;
        public static string UserPosition;
        public static string AmphurID;
        public static string ProvinceID;
        public static string AmphurName;
        public static string ProvinceName;
    }
}
