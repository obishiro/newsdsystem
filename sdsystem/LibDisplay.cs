﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;
using System.Drawing;

namespace sdsystem
{
    class LibDisplay
    {
        public void LoadYod(ComboBox cbbYod)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select id,yodname from tb_yod order by id";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbYod.ValueMember = "id";
            cbbYod.DisplayMember = "yodname";
            cbbYod.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }


        public void LoadPosition(ComboBox cbbPosition)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select id,position_name from tb_position order by id";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbPosition.ValueMember = "id";
            cbbPosition.DisplayMember = "position_name";
            cbbPosition.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }
        public void LoadScar(ComboBox cbbScar)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select id,scar_name from tb_scar order by id";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbScar.ValueMember = "id";
            cbbScar.DisplayMember = "scar_name";
            cbbScar.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }
        public void LoadNationality(ComboBox cbbNation)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select id,na_name from tb_nationality order by id";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbNation.ValueMember = "id";
            cbbNation.DisplayMember = "na_name";
            cbbNation.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }
        public void LoadOccupation(ComboBox cbbOccupation)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select id,occu_name from tb_occupation order by id";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbOccupation.ValueMember = "id";
            cbbOccupation.DisplayMember = "occu_name";
            cbbOccupation.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }
        public void LoadReligion(ComboBox cbbReligion)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select id,religion_name from tb_religion order by id";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbReligion.ValueMember = "id";
            cbbReligion.DisplayMember = "religion_name";
            cbbReligion.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }
        public void LoadEducation(ComboBox cbbEducation)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select id,education_name from tb_education order by id";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbEducation.ValueMember = "id";
            cbbEducation.DisplayMember = "education_name";
            cbbEducation.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }
        public void LoadRegisFollow(ComboBox cbbRegisfollow)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select id,regisfollow from tb_regis_follow order by id";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbRegisfollow.ValueMember = "id";
            cbbRegisfollow.DisplayMember = "regisfollow";
            cbbRegisfollow.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }

        public void GetDay(ComboBox cbbday)
        {

            Dictionary<string, string> day = new Dictionary<string, string>();
            string value;
            day.Add("0", "--กรุณาเลือก--");
            for (int i = 0; i <= 30; i++)
            {
                int ii = i + 1;
                if (ii <= 9)
                {
                    value = "0" + ii;
                }
                else
                {
                    value = "" + ii;
                }

                day.Add(value, value);
                cbbday.DataSource = new BindingSource(day, null);
                cbbday.DisplayMember = "Value";
                cbbday.ValueMember = "Key";

            }

        }
        public void GetMonth(ComboBox cbbmonth)
        {

            ////string[] dayno = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
            string[] arrayMonthName = { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฏาคม", "สิงหาคม",
               "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };

            Dictionary<string, string> arrayMonth = new Dictionary<string, string>();
            string value;

            for (int i = 0; i <= 11; i++)
            {
                int ii = i + 1;
                if (ii <= 9)
                {
                    value = "0" + ii;
                }
                else
                {
                    value = "" + ii;
                }
                arrayMonth.Add(value, arrayMonthName[i]);
            }

            cbbmonth.DataSource = new BindingSource(arrayMonth, null);
            cbbmonth.DisplayMember = "Value";
            cbbmonth.ValueMember = "Key";

        }
        public void GetYear(ComboBox cbbyear)
        {

            /*Dictionary<string, string> year = new Dictionary<string, string>();
            
            DateTime MyDate = new DateTime();
            string values;
            int Year = MyDate.Year;
            int Month = MyDate.Month;
            int Day = MyDate.Day;

            for (int i = 1988; i <= Year; i++)
            {
                int ii = i + 543;
                values = "" + ii;


                year.Add(values, values);
                cbbyear.DataSource = new BindingSource(year, null);
                */
            Dictionary<string, string> YearNow = new Dictionary<string, string>();
            string DateNow = DateTime.Now.ToString("yyyy");
            int Year = Int32.Parse(DateNow); //+ 543; //2561
            int begin = Year - 30; //2531
            int end = Year + 1;
            string value;
            for (int i = Year; i >= begin; i--)
            {

                value = "" + i;

                YearNow.Add(value, value);
                cbbyear.DataSource = new BindingSource(YearNow, null);
                cbbyear.DisplayMember = "Value";
                cbbyear.ValueMember = "Key";

            }

         
    }
     
        public void GetYearRegis(ComboBox cbbyear)
        {
            Dictionary<string, string> YearNow = new Dictionary<string, string>();
            string DateNow = DateTime.Now.ToString("yyyy");
            int Year = Int32.Parse(DateNow)-17; //2544
            int begin = Year - 13;
            int end = Year + 1;
            string value;
            for (int i = Year; i >= begin; i--)
            {

                value = "" + i;

                YearNow.Add(value, value);
                cbbyear.DataSource = new BindingSource(YearNow, null);

                cbbyear.DisplayMember = "Value";
                cbbyear.ValueMember = "Key";

            }


        }
        public string GetThaiZodiac(int txtbox)
        {
           string[] arr = { "ระกา", "จอ", "กุน", "ชวด", "ฉลู", "ขาล", "เถาะ", "มะโรง", "มะเส็ง", "มะเมีย", "มะแม", "วอก" };
            return arr[(txtbox - 4)% 12] ;
        }
        public void LoadProvince(ComboBox cbbBox,int ProvId)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = null;

            if (ProvId == 0)
            {
                 StrSql = "select PROVINCE_ID,PROVINCE_NAME from tb_province order by PROVINCE_NAME asc";

            }
            else
            {
                 StrSql = "select PROVINCE_ID,PROVINCE_NAME from tb_province where PROVINCE_ID ='"+ProvId+"' ";

            }



            
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbBox.ValueMember = "PROVINCE_ID";
            cbbBox.DisplayMember = "PROVINCE_NAME";
            cbbBox.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }
        public void LoadAumphur(ComboBox cbbBox, string ProvId)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = null;
            if (ProvId == "0")
            {
                StrSql = "select AMPHUR_ID,AMPHUR_NAME from tb_amphur order by AMPHUR_NAME ASC ";

            }
            else
            {
                StrSql = "select AMPHUR_ID,AMPHUR_NAME from tb_amphur where PROVINCE_ID ='" + ProvId + "' order by AMPHUR_NAME ASC ";

            }


            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbBox.ValueMember = "AMPHUR_ID";
            cbbBox.DisplayMember = "AMPHUR_NAME";
            cbbBox.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }
        public void LoadTumbon(ComboBox cbbBox, string AmphurId)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = null;
            if(AmphurId =="0")
            {
                StrSql = "select DISTRICT_ID,DIStrICT_NAME from tb_district  order by DISTRICT_NAME ASC ";

            }
            else
            {
                StrSql = "select DISTRICT_ID,DIStrICT_NAME from tb_district where AMPHUR_ID ='" + AmphurId + "' order by DISTRICT_NAME ASC ";

            }



            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbBox.ValueMember = "DISTRICT_ID";
            cbbBox.DisplayMember = "DISTrICT_NAME";
            cbbBox.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }
        public void LoadPostcode(TextBox txtBox, string AmphurId)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = null;

            StrSql = "select ZIPCODE from tb_zipcode where AMPHUR_ID ='" + AmphurId + "' ";


            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                txtBox.Text = "" + rdr["ZIPCODE"];
            }
            rdr.Close();
            // string zipcode = ""+ rdr["ZIPCODE"];
            // 



            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));

            }*/

        }
        public void GetMoo(ComboBox cbbday)
        {

            Dictionary<string, string> day = new Dictionary<string, string>();
            string value;
            day.Add("0", "--กรุณาเลือก--");
            for (int i = 1; i <= 30; i++)
            {
                
                    value = "" + i;
               
                
                day.Add(value, value);
                cbbday.DataSource = new BindingSource(day, null);
                cbbday.DisplayMember = "Value";
                cbbday.ValueMember = "Key";

            }

        }
        public void GetMooBanName(TextBox txtBox,string moo,string tumbon)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = null;

            StrSql = "select ban from tb_configmooban where moo ='" + moo + "' and tumbon='"+tumbon+"' ";


            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                while (rdr.Read())
                {
                    txtBox.Text = "" + rdr["ban"];
                }
            }
            else
            {
                txtBox.Text = "-";
            }
            rdr.Close();



        }
        public void GetBookNumber (TextBox textBox,string numberstring)
        {
            if (numberstring == "")
            {
                textBox.Text = "";
            }
            else
            {
                int number = Int32.Parse(numberstring);
                if (number <= 100)
                {
                    textBox.Text = "1";
                }
                else if (number >= 101 && number <= 200)
                {
                    textBox.Text = "2";
                }
                else if (number >= 201 && number <= 300)
                {
                    textBox.Text = "3";
                }
                else if (number >= 301 && number <= 400)
                {
                    textBox.Text = "4";
                }
                else if (number >= 401 && number <= 500)
                {
                    textBox.Text = "5";
                }
                else if (number >= 501 && number <= 600)
                {
                    textBox.Text = "6";
                }
                else if (number >= 601 && number <= 700)
                {
                    textBox.Text = "7";
                }
                else if (number >= 701 && number <= 800)
                {
                    textBox.Text = "8";
                }
                else if (number >= 801 && number <= 900)
                {
                    textBox.Text = "9";
                }
                else if (number >= 901 && number <= 1000)
                {
                    textBox.Text = "10";
                }
                else if (number >= 1001 && number <= 1100)
                {
                    textBox.Text = "11";
                }
                else if (number >= 1101 && number <= 1200)
                {
                    textBox.Text = "12";
                }
                else if (number >= 1201 && number <= 1300)
                {
                    textBox.Text = "13";
                }
                else if (number >= 1301 && number <= 1400)
                {
                    textBox.Text = "14";
                }
                else if (number >= 1401 && number <= 1500)
                {
                    textBox.Text = "15";
                }
                else if (number >= 1501 && number <= 1600)
                {
                    textBox.Text = "16";
                }
                else if (number >= 1601 && number <= 1700)
                {
                    textBox.Text = "17";
                }
                else if (number >= 1701 && number <= 1800)
                {
                    textBox.Text = "18";
                }
                else if (number >= 1801 && number <= 1900)
                {
                    textBox.Text = "19";
                }
                else if (number >= 1901 && number <= 2000)
                {
                    textBox.Text = "20";
                }
                else if (number >= 2001 && number <= 2100)
                {
                    textBox.Text = "21";
                }
                else if (number >= 2101 && number <= 2200)
                {
                    textBox.Text = "22";
                }
                else if (number >= 2201 && number <= 2300)
                {
                    textBox.Text = "23";
                }
                else if (number >= 2301 && number <= 2400)
                {

                    textBox.Text = "24";
                }
                else if (number >= 2401 && number <= 2500)
                {
                    textBox.Text = "25";

                }
                else if (number >= 2501 && number <= 2600)
                {
                    textBox.Text = "26";

                }
                else if (number >= 2601 && number <= 2700)
                {
                    textBox.Text = "27";

                }
                else if (number >= 2701 && number <= 2800)
                {
                    textBox.Text = "28";

                }
                else if (number >= 2801 && number <= 2900)
                {
                    textBox.Text = "29";

                }
                else if (number >= 2901 && number <= 3000)
                {
                    textBox.Text = "30";

                }
                else if (number >= 3101 && number <= 3200)
                {
                    textBox.Text = "31";

                }
                else if (number >= 3201 && number <= 3300)
                {
                    textBox.Text = "32";

                }
                else if (number >= 3301 && number <= 3400)
                {
                    textBox.Text = "33";

                }
                else if (number >= 3401 && number <= 3500)
                {
                    textBox.Text = "34";

                }
                else if (number >= 3501 && number <= 3600)
                {
                    textBox.Text = "35";

                }
                else
                {
                    MessageBox.Show("เลขที่กรอกมาไม่ถูกต้อง หรือ เกินกว่าจำนวนเล่มที่กำหนดไว้", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox.Text = "";
                }
            }
        }
        public void LoadSDPosition(ComboBox cbbBox)
        {
            LibSQL Connection = new LibSQL();

            Connection.OpenConnection();
            string StrSql = "select id,position_name from tb_position where id > 2 order by id asc ";
            MySqlCommand cmd = new MySqlCommand(StrSql, Connection.connection);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);

            DataRow dr;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--กรุณาเลือก--" };
            dt.Rows.InsertAt(dr, 0);
            cbbBox.ValueMember = "id";
            cbbBox.DisplayMember = "position_name";
            cbbBox.DataSource = dt;
            /*while (rdr.Read())
            {
                cbbYod.Items.Add(rdr.GetValue(1));
               
            }*/
        }

    }
}
