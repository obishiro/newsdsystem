﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sdsystem
{
    public partial class MainForm : Form
    {
        private int childFormNumber = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            /*Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();*/
        }



        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ท่านต้องการออกจากระบบจริงหรือไม่", "ยืนยันออกจากระบบ", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {

            }
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }



     

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void fileMenu_Click(object sender, EventArgs e)
        {

        }

        private void ConfigSDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form FrmConfigSd = new FrmConfigSD();
            FrmConfigSd.MdiParent = this;
            FrmConfigSd.Show();
            //FrmConfigSd.WindowState = FormWindowState.Maximized;
        }

        private void ConfigPersonStripMenuItem_Click(object sender, EventArgs e)
        {
            /* Form frmShowConfigPerson = new FrmConfigPerson();
            frmShowConfigPerson.MdiParent = this;
            frmShowConfigPerson.Show();*/
        }

        private void BtConfigPersonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmShowConfigPerson = new FrmConfigPerson();
            frmShowConfigPerson.MdiParent = this;
            frmShowConfigPerson.Show();
        }

       

        private void MainForm_Load(object sender, EventArgs e)
        {
            Form frmRegister44 = new FrmRegister44();
            frmRegister44.MdiParent = this;
            frmRegister44.Show();
            this.StatusUser.Text = "เข้าใช้ระบบโดย " + LoginInfo.UserName+" ตำแหน่ง "+LoginInfo.UserPosition;
          //  this.StatusUser.Image = 
            //this.StatusDateTime.Text = "วันที่ " + DateTime.Today.ToLongDateString();




        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.StatusDateTime.Text = " ณ วันที่ " + DateTime.Now.ToString("d MMMM yyyy " + "เวลา" + " H:mm:ss ", CultureInfo.CreateSpecificCulture("th-TH"));
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ท่านต้องการออกจากระบบจริงหรือไม่", "ยืนยันออกจากระบบ", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {

            }
        }

        private void MenuBtregister_Click(object sender, EventArgs e)
        {
            Form frmRegister44 = new FrmRegister44();
            frmRegister44.MdiParent = this;
            frmRegister44.Show();
        }

        private void btZodiacToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmZ = new FrmZodiac();
            frmZ.MdiParent = this;
            frmZ.Show();
        }

        private void BtHomeNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form FrmBan = new FrmHomeName();
            FrmBan.MdiParent = this;
            FrmBan.Show();
        }
    }
}